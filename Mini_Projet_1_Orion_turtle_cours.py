from random import *
from turtle import *
import turtle

setup(600,500)
title("Pierre Feuille Ciseaux")
tr = turtle.Turtle()
wn = turtle.Screen()

scO = 0 #score de l'ordinateur
scP = 0 #score du joueur
while scO != 3 and scP != 3:
    Pl = ""
    while Pl != 1 and Pl != 2 and Pl != 3:
        Pl = textinput("","votre coup : ") #coup défini par le joueur
        Or = randint(1,3) # coup défini par l'ordinateur
        reset()
        hideturtle()
        up()
        if Pl == "p":
            text = "pierre"
            Pl = 1
            wn.bgpic("pierre.png")
        elif Pl == "f":
            text = "feuille"
            Pl = 2
            wn.bgpic("feuille.png")
        elif Pl == "c":
            text = "ciseaux"
            Pl = 3
            wn.bgpic("ciseaux.png")
        
    goto(-200,110)
    write(text , align= "center" , font=("Arial", 16, "bold"))
    
    if Or == 1:
        text = "pierre" # variable pour l'affichage des chois
        wn.addshape("pierre.gif")
        tr.shape("pierre.gif")
    elif Or == 2:
        text = "feuille"
        wn.addshape("feuille.gif")
        tr.shape("feuille.gif")
    else:
        text = "ciseaux"
        wn.addshape("ciseaux.gif")
        tr.shape("ciseaux.gif")
    
    goto(200,110)
    write(text , align= "center" , font=("Arial", 16, "bold"))
    
    if Pl == Or:
        goto(0,120)
        write("Egalité" , align= "center" , font=("Arial", 16, "bold"))
    elif Pl == 1 and Or == 2:
        goto(0,120)
        write("Défaite" , align= "center" , font=("Arial", 16, "bold"))
        scO += 1
    elif Pl == 2 and Or == 1:
        goto(0,120)
        write("Victoire" , align= "center" , font=("Arial", 16, "bold"))
        scP += 1
    elif Pl == 2 and Or == 3:
        goto(0,120)
        write("Défaite" , align= "center" , font=("Arial", 16, "bold"))
        scO += 1
    elif Pl == 3 and Or == 2:
        goto(0,120)
        write("Victoire" , align= "center" , font=("Arial", 16, "bold"))
        scP += 1
    elif Pl == 3 and Or == 1:
        goto(0,120)
        write("Défaite" , align= "center" , font=("Arial", 16, "bold"))
        scO += 1
    elif Pl == 1 and Or == 3:
        goto(0,120)
        write("Victoire" , align= "center" , font=("Arial", 16, "bold"))
        scP += 1
    
    goto(-200,150)
    write("score :"+str(scP), align= "center" , font=("Arial", 16, "bold"))
    goto(200,150)
    write("score :"+str(scO), align= "center" , font=("Arial", 16, "bold"))
if scP == 3:
    goto(0,175)
    write("vous avez gagné la partie!!", align= "center" , font=("Arial", 20, "bold"))
else:
    goto(0,175)
    write("vous avez perdu la partie!!", align= "center" , font=("Arial", 20, "bold"))
exitonclick()